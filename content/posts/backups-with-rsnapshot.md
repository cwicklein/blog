---
title: "Backups with rsnapshot"
subtitle: ""
date: 2019-01-21T10:47:32-06:00
tags: [rsync,rsnapshot,backups]
---

Having recently replaced an aging MacBook Pro with a Dell XPS 13, I've
had to address several needs which were automatically managed by macOS
but require some additional effort with Linux.  One such need is
making back-ups.  My strategy is to use `git` for tracking structured
changes, `rsnapshot` to keep periodic snapshots on my laptop, and
`rsync` to copy snapshots to an external drive.

# Install and configure `rsnapshot`

Install `rsnapshot` and modify its configuration file as appropriate.
Here are the changes I've made to `/etc/rsnapshot.conf` since it was
installed as part of the Fedora `rsnapshot` package.  NB
`snapshot_root` is a subdirectory of a backed-up file system, so it
must be explicitly excluded.

~~~~
--- rsnapshot.conf.orig	2018-07-15 02:16:59.000000000 -0500
+++ rsnapshot.conf	2019-01-22 13:27:00.922013753 -0600
@@ -20,7 +20,8 @@
 
 # All snapshots will be stored under this root directory.
 #
-snapshot_root	/.snapshots/
+#snapshot_root	/.snapshots/
+snapshot_root	/home/snapshots/
 
 # If no_create_root is enabled, rsnapshot will not automatically create the
 # snapshot_root directory. This is particularly useful if you are backing
@@ -54,7 +55,7 @@
 
 # Comment this out to disable syslog support.
 #
-cmd_logger	/usr/bin/logger
+#cmd_logger	/usr/bin/logger
 
 # Uncomment this to specify the path to "du" for disk usage checks.
 # If you have an older version of "du", you may also want to check the
@@ -90,9 +91,12 @@
 # e.g. alpha, beta, gamma, etc.         #
 #########################################
 
-retain	alpha	6
-retain	beta	7
-retain	gamma	4
+#retain	alpha	6
+#retain	beta	7
+#retain	gamma	4
+retain	daily	7
+retain	weekly	4
+retain	monthly	6
 #retain	delta	3
 
 ############################################
@@ -223,9 +227,10 @@
 ###############################
 
 # LOCALHOST
-backup	/home/		localhost/
-backup	/etc/		localhost/
-backup	/usr/local/	localhost/
+backup	/home		localhost/	exclude=/home/snapshots
+backup	/		localhost/	one_fs=1
+#backup	/etc/		localhost/
+#backup	/usr/local/	localhost/
 #backup	/var/log/rsnapshot		localhost/
 #backup	/etc/passwd	localhost/
 #backup	/home/foo/My Documents/		localhost/
~~~~

# Run `rsnapshot`

I run `rsnapshot` from this script below which checks the mtime on
each interval's most recent snapshot to determine whether or not
`rsnapshot` is due to run.  The offset `backup_time_allowance_min` is
necessary because snapshots are completed slightly *fewer* than
24-hours before the next scheduled run.

    #!/bin/bash

    ##
    ## This script should be run daily to catch-up creation of daily, weekly,
    ## and monthly backups.
    ##
    
    backup_time_allowance_min=360

    make_backup () {
        name="$1"
        days="$2"
        minutes="$(( days * 1440 - backup_time_allowance_min ))"
        if ! find /home/snapshots -maxdepth 1 -mindepth 1 -mmin -"$minutes" -name "$name".0 | grep "$name".0 >/dev/null; then
            /usr/bin/rsnapshot "$name"
            if test -d /home/snapshots/"$name".0; then
                touch /home/snapshots/"$name".0
            fi
        fi
    }

    make_backup daily 1
    make_backup weekly 7 
    make_backup monthly 30

# Schedule `rsnapshot`

First, we need a service unit defined by
`/etc/systemd/system/rsnapshot.service` to run
`/usr/bin/rsnapshot.sh`.

~~~~
[Unit]
Description=rsnapshot backup script

[Service]
Type=oneshot
Nice=19
IOSchedulingClass=3
ExecStart=/usr/bin/rsnapshot.sh
~~~~

Second, we need a timer unit defined by
`/etc/systemd/system/rsnapshot.timer` to periodically start the
service.  Most days, I'm using my laptop by 05:30, so that's a good
time for me.  Because the timer unit is persistent, the backup script
will run when it can if it missed its last start time.

~~~~
[Unit]
Description=rsnapshot backup script

[Timer]
OnCalendar=05:30
Persistent=true
Unit=rsnapshot.service

[Install]
WantedBy=timers.target
~~~~

# Copying snapshots to an external drive

After connecting an external USB drive, I run this script.  It is
necessary to first create the host-specific snapshot root on the
external drive.  This script isn't run automatically (what happens if
I'm connecting the drive to *restore* snapshots which were
inadvertently removed from my laptop.)  By checking that a specific
directory hierarchy exists on the back-up drive, I don't need to worry
about tracking any given back-up drive's UUID.

    #!/bin/bash

    sudo bash -c '
    for target in $(find /run/media/$USER -maxdepth 3 -mindepth 3 -path "/*/snapshots/$HOSTNAME"); do
        rsync -az -H --delete --numeric-ids /home/snapshots/* "$target"
    done
