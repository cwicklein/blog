---
title: "Unifi Ap Mesh Multicast"
date: 2020-01-31T19:55:47-06:00
draft: true
---

Replacing Google Wifi with a [custom
router](https://protectli.com/protectli-4-port/) and [UniFi
APs](https://www.ui.com/unifi/unifi-ap-ac-lr/) was not without
wrinkles.  At best, for a given VLAN carried by multiple APs,
multicast is only reliable between clients associated with the same
AP.  This breaks mDNS, and it means that if your AirPrint-capable
printer is associated with a different AP than your iPhone, you're not
going to be printing.

Fortunately, multicast datagrams *are* back-hauled across the mesh to
a port on the router, so it's possible to fix things there.  My
solution has been to run software on the router which sends periodic
unicast IGMP group membership queries to each neighbor on the wireless
VLAN, receives their membership reports, and resends multicast
datagrams unicast to each group member.

My first cut at a multicast to unicast publisher is a Julia program
which sends membership queries when run with the `--query` option and
processes membership reports and resends multicast datagrams when run
with the `--forward` option.  This division allows each process to be
implemented as a simple loop.  The code below includes from a local
copy of [`Pcap.jl`](https://github.com/c-wicklein/Pcap.jl) which I've
forked to add more `libpcap` support.

In its present form, this solution is good enough to fix mDNS, but I
plan to add support for leave group messages, expiring group
memberships, and possibly group filtering.

```julia
using ArgParse
using Printf
using Sockets

include("Pcap.jl/src/Pcap.jl")

IPPROTO_IGMP = 2
IGMP_MEMBERSHIP_QUERY = 0x11
IGMP_V1_MEMBERSHIP_REPORT = 0x12
IGMP_V2_MEMBERSHIP_REPORT = 0x16

SNAPLEN = 4096

mutable struct Ethernet
    addr::Vector{UInt8}

    function Ethernet(a::Vector{UInt8})
        new(a[1:6])
    end    
    
    function Ethernet(a::UInt8, b::UInt8, c::UInt8, d::UInt8, e::UInt8, f::UInt8)
        Ethernet([a, b, c, d, e, f])
    end
    
    function Ethernet(s::AbstractString)
        Ethernet(map(x->parse(UInt8, x, base=16), split(s, ':')))
    end
end

Base.show(io::IO, eth::Ethernet) = print(io, "eth\"", eth, "\"")
Base.print(io::IO, eth::Ethernet) = print(io, @sprintf("%02x:%02x:%02x:%02x:%02x:%02x", eth.addr[1], eth.addr[2], eth.addr[3], eth.addr[4], eth.addr[5], eth.addr[6]))

struct ArpEntry
    ip::IPv4
    iface::String
    mac::Ethernet

    function ArpEntry(ip::IPv4, iface::AbstractString, mac::Ethernet)
        new(ip, iface, mac)
    end

    function ArpEntry(ip::AbstractString, iface::AbstractString, mac::AbstractString)
        new(IPv4(ip), iface, Ethernet(mac))
    end
end

function load_arp_table()
    ret = Array{ArpEntry, 1}()
    open("/proc/net/arp") do s
        lines = readlines(s)
        if length(lines) > 1
            for ln in lines[2:end]
                ary = split(ln)
                if ary[4] != "00:00:00:00:00:00"
                    push!(ret, ArpEntry(ary[1], ary[6], ary[4]))
                end
            end
        end
    end
    ret
end

function ip4_addr_as_bytes(ip)
    map(x->convert(UInt8, x),
        [(ip.host & 0xff000000) >> 24,
         (ip.host & 0x00ff0000) >> 16,
         (ip.host & 0x0000ff00) >> 8,
         (ip.host & 0x000000ff)])
end

function ip_cksum(data)
    sum::UInt16 = 0x0000
    for offset in range(0, stop=length(data)-2, step=2)
        v::UInt16 = 256 * data[offset+1] + data[offset+2]
        sum, overflow = Base.add_with_overflow(sum, v)
        if overflow
            sum += 1
        end
    end
    ~sum
end

function make_query_packet(iface)
    packet = fill(0x00, 42)
    offset = 0

    ether_dst_addr = Ethernet("01:00:5e:00:00:01")
    packet[offset+1:offset+6] .= ether_dst_addr.addr # destination ethernet address
    ether_src_addr = ether_addr_for_iface(iface)
    packet[offset+7:offset+12] .= ether_src_addr.addr # source ethernet address
    packet[offset+13:offset+14] .= [0x08, 0x00] # ethernet protocol
    offset += 14

    packet[offset+1] = (packet[offset+1] & 0x0f) | (4 << 4) # IPv4
    packet[offset+1] = (packet[offset+1] & 0xf0) | (20 ÷ 4) # length in 32-bit words
    packet[offset+3] = (convert(UInt16, length(packet) - offset) & 0xff00) >> 8 # packet length, high-order byte
    packet[offset+4] = convert(UInt16, length(packet) - offset) & 0x00ff # packet length, low-order byte
    packet[offset+9] = 0x01 # time to live
    packet[offset+10] = IPPROTO_IGMP
    ip4_src_addr = ip4_addr_for_iface(iface)
    packet[offset+13:offset+16] = ip4_addr_as_bytes(ip4_src_addr)
    ip4_dst_addr = @ip_str "224.0.0.1"
    packet[offset+17:offset+20] = ip4_addr_as_bytes(ip4_dst_addr)
    cksum = ip_cksum(packet[offset+1:offset+20])
    packet[offset+11] = (cksum & 0xff00) >> 8
    packet[offset+12] = cksum & 0x00ff
    offset += 20

    packet[offset+1] = IGMP_MEMBERSHIP_QUERY
    packet[offset+2] = 0 # max response time
    mcast_group = @ip_str "0.0.0.0"
    packet[offset+5:offset+8] = ip4_addr_as_bytes(mcast_group)
    cksum = ip_cksum(packet[offset+1:offset+8])
    packet[offset+3] = (cksum & 0xff00) >> 8
    packet[offset+4] = cksum & 0x00ff
    
    packet
end

function query_loop(iface)
    packet = make_query_packet(iface)
    handle = Pcap.pcap_open_live(iface, SNAPLEN, 0, 0)

    while true
        for neighbor in load_arp_table()
            if neighbor.iface == iface
                packet[1:6] .= neighbor.mac.addr
                bytes_sent = Pcap.pcap_inject(handle, packet)
            end
        end
        sleep(60)
    end
end

function forward_loop(iface)
    handle = Pcap.pcap_open_live(iface, SNAPLEN, 0, 0)
    program = Pcap.pcap_compile(handle, "ip and (igmp or multicast)")
    Pcap.pcap_setfilter(handle, program)
    memberships::Dict{Vector{UInt8}, Set{Vector{UInt8}}} = Dict()

    while true
        (result, header, packet) = Pcap.pcap_next_ex(handle)
        if result != 1
            break
        end
        offset = 14 # pass the Ethernet header
        if packet[offset+10] == IPPROTO_IGMP # IPv4 IGMP
            ip_hdr_len = 4 * (packet[offset+1] & 0x0f)
            offset += ip_hdr_len # pass the variable length IP header
            if packet[offset+1] == IGMP_V1_MEMBERSHIP_REPORT || packet[offset+1] == IGMP_V2_MEMBERSHIP_REPORT
                group_addr = packet[offset+5:offset+8]
                offset -= (ip_hdr_len + 14) # back to the Ethernet header
                ether_src_addr = packet[offset+7:offset+12]
                if group_addr ∉ keys(memberships)
                    memberships[group_addr] = Set([ether_src_addr])
                else
                    push!(memberships[group_addr], ether_src_addr)
                end
            end
        else # IPv4 multicast
            group_addr = packet[offset+17:offset+20]
            if group_addr ∈ keys(memberships)
                offset = 0
                for ether_dst_addr in memberships[group_addr]
                    packet[offset+1:offset+6] .= ether_dst_addr
                    bytes_sent = Pcap.pcap_inject(handle, packet)
                end
            end
        end
    end
end

function ether_addr_for_iface(iface)
    return Ethernet(rstrip(read("/sys/class/net/$(iface)/address", String)))
end

function ip4_addr_for_iface(iface)
    ret = nothing
    open(`ip a show dev $iface`, "r", stdin) do s
        for ln in eachline(s)
            m = match(r"^[ ]+inet (.*)? brd", ln)
            if m != nothing
                ret = IPv4(split(m[1], "/")[1])
                break
            end
        end
    end
    ret
end

function parse_commandline()
    s = ArgParseSettings()

    @add_arg_table s begin
        "--interface"
        help = "Operate on this interface"
        required = true
        
        "--query"
        action = :store_const
        constant = true
        default = false
        help = "Send IGMP membership queries"

        "--forward"
        action = :store_const
        constant = true
        default = false
        help = "Handle IGMP responses and resend multicast datagrams"
    end

    parse_args(s)
end

function main()
    args = parse_commandline()
    if ! (args["query"] ⊻ args["forward"])
        @warn("The options --query and --forward are mutually exclusive.")
        exit(1)
    end

    if args["query"]
        query_loop(args["interface"])
    elseif args["forward"]
        forward_loop(args["interface"])
    else
        exit(1)
    end
end

main()
